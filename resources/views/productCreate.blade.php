@extends('layouts.main')

@section('title') Welcome @endsection

@section('content')
    <form class="mt-50" method="post" action="{{ route('action.product.create') }}">
        @csrf
        <h1 class="text-center p-5">Creaza produs</h1>
        <div>
            <div class="row p-4">
                <div class="col-md-12 p-2">
                    <label for="name" class="mb-2">Nume</label>
                    <input required  type="text" value="{{ ($product ?? [])->name ?? '' }}" class="p-1 form-control" id="name"
                        name="name">
                </div>
                <div class="col-md-12 p-2">
                    <label for="title" class="mb-2">Titlul</label>
                    <input required  type="text" value="{{ ($product ?? [])->title ?? '' }}" class="p-1 form-control" id="title"
                        name="title">
                </div>
                <div class="col-md-12 p-2">
                    <label for="photo_url" class="mb-2 ">Link la foto</label>
                    <input required  type="text" value="{{ $product ?? ([])->photo_url ?? '' }}" class="p-1 form-control"
                        id="photo_url" name="photo_url">
                </div>
                <div class="col-md-12 p-2">
                    <label for="price" class="mb-2 ">Pret</label>
                    <input required  type="number" value="{{ ($product ?? [])->price ?? '' }}" class="p-1 form-control" id="price"
                        name="price">
                </div>
                <div class="col-md-12 p-2">
                    <label for="description" class="mb-2 ">Informatia</label>
                    <input required type="text" value="{{ ($product ?? [])->information ?? '' }}" class="p-1 form-control"
                        id="information" name="information">
                </div>
                <div class="col-md-12 p-2">
                    <label for="description" class="mb-2 ">Descrierea</label>
                    <input required type="text" value="{{ ($product ?? [])->description ?? '' }}" class="p-1 form-control"
                        id="description" name="description">
                </div>
                <div class="col-md-12 p-2 text-center mt-5">
                    <button type="submit" class="btn m-3 btn-primary">Salveaza Produsul</button>
                </div>
            </div>
        </div>
    </form>
@endsection
